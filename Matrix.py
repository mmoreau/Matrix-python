import sys


class Matrix:



	def __init__(self):
		pass



	@classmethod
	def Disp(cls, matrix, sep=True):

		"""Displays the matrix."""

		lock = 0


		if isinstance(matrix, list):
			lock += 1


		if isinstance(sep, bool):
			lock += 1


		if lock == 2:

			for i in range(0, len(matrix)):
				for j in range(0, len(matrix[0])):
					sys.stdout.write(f"{matrix[i][j]} ")
				sys.stdout.write("\n")

			if sep:
				sys.stdout.write("\n")





	@classmethod
	def T(cls, matrix):

		"""Transposes the matrix."""

		if isinstance(matrix, list):

			t, tmp = [], []

			for i in range(0, len(matrix[0])):
				for j in range(0, len(matrix)):
					tmp.append(matrix[j][i])

				t.append(tmp)
				tmp = []

			return t



	@classmethod
	def Mul(cls, M1, M2):

		"""Multiplies two matrices."""

		lock = 0


		if isinstance(M1, list):
			lock += 1


		if isinstance(M2, list):
			lock += 1


		if lock == 2:

			lengthM1Y = len(M1)
			lengthM2Y = len(M2)

			if lengthM1Y > 0 and lengthM2Y > 0:

				isMatrixSquare = False

				for i in range(0, lengthM1Y):

					if (lengthM1Y ** 2) == (lengthM1Y * len(M1[i])):
						if (lengthM1Y ** 2) == (lengthM2Y * len(M2[i])):
							isMatrixSquare = True
					else:
						isMatrixSquare = False
						break


				if isMatrixSquare:

					rMatrix, tmp = [], []
					count = 0

					for i in range(0, lengthM1Y):
						for k in range(0, lengthM1Y):
							for j in range(0, lengthM1Y):

								if j == (lengthM1Y - 1):
									count += (M1[i][j] * M2[j][k])
									tmp.append(count)
									count = 0
								else:
									count += (M1[i][j] * M2[j][k])

						rMatrix.append(tmp)
						tmp = []

					return rMatrix



	@classmethod
	def Div(cls, M1, M2):

		"""Divide two matrices."""

		lock = 0


		if isinstance(M1, list):
			lock += 1


		if isinstance(M2, list):
			lock += 1


		if lock == 2:

			lengthM1Y = len(M1)
			lengthM2Y = len(M2)

			if lengthM1Y > 0 and lengthM2Y > 0:

				isMatrixSquare = False

				for i in range(0, lengthM1Y):

					if (lengthM1Y ** 2) == (lengthM1Y * len(M1[i])):
						if (lengthM1Y ** 2) == (lengthM2Y * len(M2[i])):
							if 0 in M2[i]:
								isMatrixSquare = False
								break
							else:
								isMatrixSquare = True
					else:
						isMatrixSquare = False
						break


				if isMatrixSquare:

					rMatrix, tmp = [], []
					count = 0

					for i in range(0, lengthM1Y):
						for k in range(0, lengthM1Y):
							for j in range(0, lengthM1Y):

								if j == (lengthM1Y - 1):
									count += (M1[i][j] / M2[j][k])
									tmp.append(count)
									count = 0
								else:
									count += (M1[i][j] / M2[j][k])

						rMatrix.append(tmp)
						tmp = []

					return rMatrix



	@classmethod
	def Add(cls, M1, M2):

		"""Adds two matrices."""

		lock = 0


		if isinstance(M1, list):
			lock += 1
		

		if isinstance(M2, list):
			lock += 1
		

		if lock == 2:

			M1Len = len(M1)
			M2Len = len(M2)

			if M1Len == M2Len:

				state = True

				for i in range(0, M1Len):
					if len(M1[i]) != len(M2[i]):
						state = False
						break

				if state:
					
					stockMatrix = []

					for i in range(0, M1Len):

						tmp = []

						for j in range(0, len(M1[0])):
							tmp.append((M1[i][j] + M2[i][j]))

						stockMatrix.append(tmp)

					return stockMatrix



	@classmethod
	def Sub(cls, M1, M2):

		"""Subtracts two matrices."""

		lock = 0


		if isinstance(M1, list):
			lock += 1
		

		if isinstance(M2, list):
			lock += 1
		

		if lock == 2:

			M1Len = len(M1)
			M2Len = len(M2)

			if M1Len == M2Len:

				state = True

				for i in range(0, M1Len):
					if len(M1[i]) != len(M2[i]):
						state = False
						break

				if state:
					
					stockMatrix = []

					for i in range(0, M1Len):

						tmp = []

						for j in range(0, len(M1[0])):
							tmp.append((M1[i][j] - M2[i][j]))

						stockMatrix.append(tmp)

					return stockMatrix