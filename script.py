import os
import sys
from Matrix import Matrix

try:
	A = [
		[1, 0, 0],
		[1, 0, 0],
		[1, 1, 0]
	]


	B = [
		[0, 0, 1],
		[0, 1, 0],
		[1, 0, 0]
	]


	print(".:: Addition ::.\n")
	Matrix.Disp(Matrix.Add(A, B))

	print(".:: Multiplication ::.\n")
	Matrix.Disp(Matrix.Mul(A, B))

	print(".:: Subtraction ::.\n")
	Matrix.Disp(Matrix.Sub(A, B))


	print(".:: Division ::.\n")

	"""
	In this example, when dividing, there is no result in return, according to the rules and 
	formulas in mathematics, we cannot divide by 0, in case of 0 we cancel the calculation, 
	the matrix is not calculable
	"""

	Matrix.Disp((Matrix.Div(A, B)))
except:
	pass

if sys.platform.startswith("win32"):
	os.system("pause")